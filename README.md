# minimal-poc

A minimal project to illustrate how to integrate Gitlab-CI and Docker containers  
and orchestrate launching multiple services on a server vi CI


### the simplest possible POC

Let's prove this out with something very simple - the code from
     https://github.com/hellokoding/dockercompose-springboot-mysql-nginx
	 

### server setup

At some point we will make a new VCM server template, but for now I am manually 
components onto a generic Ubuntu 18.04 linux VM. Here is the worksheet:

     sudo apt-get update
     sudo apt-get dist-upgrade -y
     
     [...reboot...]
     
     ##################
     ##### Docker #####
     ##################
     
     sudo apt-get install -y 
         linux-image-extra-virtual
     
     sudo apt-get install -y \
         apt-transport-https \
         ca-certificates \
         curl \
         software-properties-common
     
     curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
     
     sudo add-apt-repository \
       "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
       $(lsb_release -cs) \
       stable"
       
     sudo apt-get update
     sudo apt-get install -y docker-ce
     
     ##########################
     ##### Docker compose #####
     ##########################
     
     sudo curl -L "https://github.com/docker/compose/releases/download/1.23.2/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
     
     sudo chmod +x /usr/local/bin/docker-compose
     
     #########################
     ##### Gitlab runner #####
     #########################
     
     curl -L https://packages.gitlab.com/install/repositories/runner/gitlab-ci-multi-runner/script.deb.sh | sudo bash
     
     sudo sh -c ' cat > /etc/apt/preferences.d/pin-gitlab-runner.pref <<EOF
     Explanation: Prefer GitLab provided packages over the Debian native ones
     Package: gitlab-ci-multi-runner
     Pin: origin packages.gitlab.com
     Pin-Priority: 1001
     EOF'
     
     sudo apt-get update
     sudo apt-get install -y gitlab-runner
     
     sudo groupadd admin
     sudo usermod -a -G admin gitlab-runner
     
     sudo sh -c "echo 'gitlab-runner ALL=(ALL) NOPASSWD:ALL' >> /etc/sudoers"
     
     sudo gitlab-runner register
     ------------------------------
     vcm@vcm-7656:/srv$ sudo gitlab-runner register
     Running in system-mode.                            
                                                        
     Please enter the gitlab-ci coordinator URL (e.g. https://gitlab.com/):
     https://gitlab.oit.duke.edu/
     Please enter the gitlab-ci token for this runner:
     478h3feh58yishtytr8h874he7hfe874h
     Please enter the gitlab-ci description for this runner:
     [vcm-7656.vm.duke.edu]: 
     Please enter the gitlab-ci tags for this runner (comma separated):
     minimal-poc
     Whether to run untagged builds [true/false]:
     [false]: 
     Whether to lock the Runner to current project [true/false]:
     [true]: 
     Registering runner... succeeded                     runner=_d4yCjG4
     Please enter the executor: kubernetes, docker-ssh, parallels, shell, docker+machine, docker, ssh, virtualbox, docker-ssh+machine:
     shell
     Runner registered successfully. Feel free to start it, but if it's running already the config should be automatically reloaded! 
     vcm@vcm-7656:/srv$ 
     -------------------------------
     
     ######### containers and run scripts
     
     sudo mkdir /srv/persistent-data
     sudo chown vcm /srv/persistent-data
     